-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: sistemaVisualizacionPortuaria
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `antecedente`
--

DROP TABLE IF EXISTS `antecedente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `antecedente` (
  `idantecedente` int(11) NOT NULL,
  `nombreAntecedente` varchar(450) NOT NULL,
  `valorAntecedente` varchar(4500) NOT NULL,
  `instalacionPortuaria` int(11) NOT NULL,
  PRIMARY KEY (`idantecedente`),
  KEY `fk_antecedente_instalacionPortuaria1_idx` (`instalacionPortuaria`),
  CONSTRAINT `fk_antecedente_instalacionPortuaria1` FOREIGN KEY (`instalacionPortuaria`) REFERENCES `instalacionPortuaria` (`idinstalacionPortuaria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `antecedente`
--

LOCK TABLES `antecedente` WRITE;
/*!40000 ALTER TABLE `antecedente` DISABLE KEYS */;
INSERT INTO `antecedente` VALUES (1500901644,'Primer Antecedente','DescripciÃ³n primer antecedente',1500901486),(1500901659,'Segundo Antecedente','Valor Segundo Antecedente',1500901486);
/*!40000 ALTER TABLE `antecedente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areaManiobra`
--

DROP TABLE IF EXISTS `areaManiobra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areaManiobra` (
  `idareaManiobra` int(11) NOT NULL,
  `nombreArea` varchar(450) NOT NULL,
  `instalacionPortuaria` int(11) NOT NULL,
  PRIMARY KEY (`idareaManiobra`),
  KEY `fk_areaManiobra_instalacionPortuaria1_idx` (`instalacionPortuaria`),
  CONSTRAINT `fk_areaManiobra_instalacionPortuaria1` FOREIGN KEY (`instalacionPortuaria`) REFERENCES `instalacionPortuaria` (`idinstalacionPortuaria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areaManiobra`
--

LOCK TABLES `areaManiobra` WRITE;
/*!40000 ALTER TABLE `areaManiobra` DISABLE KEYS */;
INSERT INTO `areaManiobra` VALUES (1500901745,'Ãrea de Maneobra InstalaciÃ³n',1500901486);
/*!40000 ALTER TABLE `areaManiobra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bahia`
--

DROP TABLE IF EXISTS `bahia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bahia` (
  `idbahia` int(11) NOT NULL,
  `nombreBahia` varchar(450) NOT NULL,
  PRIMARY KEY (`idbahia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bahia`
--

LOCK TABLES `bahia` WRITE;
/*!40000 ALTER TABLE `bahia` DISABLE KEYS */;
INSERT INTO `bahia` VALUES (1500901147,'BahÃ­a Costera ValparaÃ­so'),(1500902190,'Puerto San Antonio');
/*!40000 ALTER TABLE `bahia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bahiaDisponible`
--

DROP TABLE IF EXISTS `bahiaDisponible`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bahiaDisponible` (
  `usuario` int(11) NOT NULL,
  `bahia` int(11) NOT NULL,
  `estadoVisible` varchar(45) NOT NULL,
  PRIMARY KEY (`usuario`,`bahia`),
  KEY `fk_usuario_has_bahia_bahia1_idx` (`bahia`),
  KEY `fk_usuario_has_bahia_usuario1_idx` (`usuario`),
  CONSTRAINT `fk_usuario_has_bahia_bahia1` FOREIGN KEY (`bahia`) REFERENCES `bahia` (`idbahia`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_usuario_has_bahia_usuario1` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bahiaDisponible`
--

LOCK TABLES `bahiaDisponible` WRITE;
/*!40000 ALTER TABLE `bahiaDisponible` DISABLE KEYS */;
/*!40000 ALTER TABLE `bahiaDisponible` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caracteristica`
--

DROP TABLE IF EXISTS `caracteristica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caracteristica` (
  `idcaracteristica` int(11) NOT NULL,
  `nombreCaracteristica` varchar(450) NOT NULL,
  `valorCaracteristica` varchar(4500) NOT NULL,
  `zona` int(11) NOT NULL,
  PRIMARY KEY (`idcaracteristica`),
  KEY `fk_caracteristicaZE_zonaEspera1_idx` (`zona`),
  CONSTRAINT `fk_caracteristicaZE_zonaEspera1` FOREIGN KEY (`zona`) REFERENCES `zona` (`idzona`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caracteristica`
--

LOCK TABLES `caracteristica` WRITE;
/*!40000 ALTER TABLE `caracteristica` DISABLE KEYS */;
INSERT INTO `caracteristica` VALUES (1500901894,'EspecificaciÃ³n 1','Valor especificaciÃ³n',1500901875),(1500901962,'Zona de Espera C1','Valor caracterÃ­stica en ZE C1',1500901846),(1500901986,'CaracterÃ­stica Z2','Valor Z2',1500901855);
/*!40000 ALTER TABLE `caracteristica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caracteristicaArea`
--

DROP TABLE IF EXISTS `caracteristicaArea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caracteristicaArea` (
  `idcaracteristicaArea` int(11) NOT NULL,
  `nombreCaracteristica` varchar(450) NOT NULL,
  `valorCaracteristica` varchar(4500) NOT NULL,
  `areaManiobra` int(11) NOT NULL,
  PRIMARY KEY (`idcaracteristicaArea`),
  KEY `fk_caracteristicaArea_areaManiobra1_idx` (`areaManiobra`),
  CONSTRAINT `fk_caracteristicaArea_areaManiobra1` FOREIGN KEY (`areaManiobra`) REFERENCES `areaManiobra` (`idareaManiobra`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caracteristicaArea`
--

LOCK TABLES `caracteristicaArea` WRITE;
/*!40000 ALTER TABLE `caracteristicaArea` DISABLE KEYS */;
INSERT INTO `caracteristicaArea` VALUES (1500901766,'EspecificaciÃ³n 1','Valor especificaciÃ³n 1',1500901745),(1500901781,'EspecificaciÃ³n 2','Valor especificaciÃ³n 2',1500901745);
/*!40000 ALTER TABLE `caracteristicaArea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caracteristicaBahia`
--

DROP TABLE IF EXISTS `caracteristicaBahia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caracteristicaBahia` (
  `idcaracteristicaBahia` int(11) NOT NULL,
  `nombreCaracteristica` varchar(450) NOT NULL,
  `valorCaracteristica` varchar(4500) NOT NULL,
  `bahia` int(11) NOT NULL,
  PRIMARY KEY (`idcaracteristicaBahia`),
  KEY `fk_caracteristicaBahia_bahia1_idx` (`bahia`),
  CONSTRAINT `fk_caracteristicaBahia_bahia1` FOREIGN KEY (`bahia`) REFERENCES `bahia` (`idbahia`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caracteristicaBahia`
--

LOCK TABLES `caracteristicaBahia` WRITE;
/*!40000 ALTER TABLE `caracteristicaBahia` DISABLE KEYS */;
INSERT INTO `caracteristicaBahia` VALUES (1500901217,'Primera CaracterÃ­stica','Valor primera caracterÃ­stica',1500901147),(1500901237,'Segunda CaracterÃ­stica','Valor segunda CaracterÃ­stica',1500901147),(1500902214,'CaracterÃ­stica SA 1','Valor CaracterÃ­stica 1 SA',1500902190);
/*!40000 ALTER TABLE `caracteristicaBahia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caracteristicaFisica`
--

DROP TABLE IF EXISTS `caracteristicaFisica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caracteristicaFisica` (
  `idcaracteristicasFisica` int(11) NOT NULL,
  `nombreCaracteristica` varchar(450) NOT NULL,
  `valorCaracteristica` varchar(4500) NOT NULL,
  `instalacionPortuaria` int(11) NOT NULL,
  PRIMARY KEY (`idcaracteristicasFisica`),
  KEY `fk_caracteristicaFisica_instalacionPortuaria1_idx` (`instalacionPortuaria`),
  CONSTRAINT `fk_caracteristicaFisica_instalacionPortuaria1` FOREIGN KEY (`instalacionPortuaria`) REFERENCES `instalacionPortuaria` (`idinstalacionPortuaria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caracteristicaFisica`
--

LOCK TABLES `caracteristicaFisica` WRITE;
/*!40000 ALTER TABLE `caracteristicaFisica` DISABLE KEYS */;
INSERT INTO `caracteristicaFisica` VALUES (1500901566,'CaracterÃ­stica 1','Valor caracterÃ­stica 1 en instalaciÃ³n portuaria',1500901486),(1500901583,'CaracterÃ­stica 2','Valor caracterÃ­stica 2 en instalaciÃ³n portuaria',1500901486),(1500902460,'C1 PSA','Valor C1 PSA',1500902421),(1500902475,'C2 PSA','Valor C2 PSA',1500902421);
/*!40000 ALTER TABLE `caracteristicaFisica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentoBahia`
--

DROP TABLE IF EXISTS `documentoBahia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentoBahia` (
  `iddocumentoBahia` int(11) NOT NULL,
  `nombreDocumento` varchar(450) NOT NULL,
  `descripcion` varchar(4500) NOT NULL,
  `bahia` int(11) NOT NULL,
  PRIMARY KEY (`iddocumentoBahia`),
  KEY `fk_documentoBahia_bahia1_idx` (`bahia`),
  CONSTRAINT `fk_documentoBahia_bahia1` FOREIGN KEY (`bahia`) REFERENCES `bahia` (`idbahia`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentoBahia`
--

LOCK TABLES `documentoBahia` WRITE;
/*!40000 ALTER TABLE `documentoBahia` DISABLE KEYS */;
INSERT INTO `documentoBahia` VALUES (1500901399,'download.png','Primer documento de prueba',1500901147),(1500901426,'more.png','Segundo documento de prueba',1500901147),(1500902362,'diario.png','Valor documento de prueba',1500902190);
/*!40000 ALTER TABLE `documentoBahia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentoIP`
--

DROP TABLE IF EXISTS `documentoIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentoIP` (
  `iddocumentoIP` int(11) NOT NULL,
  `nombreDocumento` varchar(450) NOT NULL,
  `descripcionDocumento` varchar(4500) NOT NULL,
  `instalacionPortuaria` int(11) NOT NULL,
  PRIMARY KEY (`iddocumentoIP`),
  KEY `fk_documentoIP_instalacionPortuaria1_idx` (`instalacionPortuaria`),
  CONSTRAINT `fk_documentoIP_instalacionPortuaria1` FOREIGN KEY (`instalacionPortuaria`) REFERENCES `instalacionPortuaria` (`idinstalacionPortuaria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentoIP`
--

LOCK TABLES `documentoIP` WRITE;
/*!40000 ALTER TABLE `documentoIP` DISABLE KEYS */;
INSERT INTO `documentoIP` VALUES (1500901693,'personal.png','Documento de prueba',1500901486),(1500902571,'guia8.pdf','Documento de Prueba',1500902421);
/*!40000 ALTER TABLE `documentoIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elementFTIP`
--

DROP TABLE IF EXISTS `elementFTIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elementFTIP` (
  `idelementFTIP` int(11) NOT NULL,
  `nombreElemento` varchar(450) NOT NULL,
  `valorElemento` varchar(4500) NOT NULL,
  `fichaTecnicaIP` int(11) NOT NULL,
  PRIMARY KEY (`idelementFTIP`),
  KEY `fk_elementFTIP_fichaTecnicaIP1_idx` (`fichaTecnicaIP`),
  CONSTRAINT `fk_elementFTIP_fichaTecnicaIP1` FOREIGN KEY (`fichaTecnicaIP`) REFERENCES `fichaTecnicaIP` (`fichaTecnicaIP`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elementFTIP`
--

LOCK TABLES `elementFTIP` WRITE;
/*!40000 ALTER TABLE `elementFTIP` DISABLE KEYS */;
INSERT INTO `elementFTIP` VALUES (1500901520,'Elemento 1','Primer elemento en ficha tÃ©cnica',1500901486),(1500901536,'Elemento 2','Segundo elemento en ficha tÃ©cnica',1500901486),(1500902439,'E1 PSA','Valor E1 PSA',1500902421);
/*!40000 ALTER TABLE `elementFTIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elementoFichaBahia`
--

DROP TABLE IF EXISTS `elementoFichaBahia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elementoFichaBahia` (
  `idelementoFichaBahia` int(11) NOT NULL,
  `nombreElemento` varchar(450) NOT NULL,
  `valorElemento` varchar(4500) NOT NULL,
  `fichaTecnicaBahia` int(11) NOT NULL,
  PRIMARY KEY (`idelementoFichaBahia`),
  KEY `fk_elementoFichaBahia_fichaTecnicaBahia1_idx` (`fichaTecnicaBahia`),
  CONSTRAINT `fk_elementoFichaBahia_fichaTecnicaBahia1` FOREIGN KEY (`fichaTecnicaBahia`) REFERENCES `fichaTecnicaBahia` (`idfichaTecnicaBahia`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elementoFichaBahia`
--

LOCK TABLES `elementoFichaBahia` WRITE;
/*!40000 ALTER TABLE `elementoFichaBahia` DISABLE KEYS */;
INSERT INTO `elementoFichaBahia` VALUES (1500901260,'Elemento 1','Valor Elemento 1',1500901147),(1500901272,'Elemento 2','Valor Elemento 2',1500901147),(1500901283,'Elemento 3','Valor elemento 3',1500901147),(1500902310,'Ficha TÃ©cnica SA 1','Elemento 1',1500902190),(1500902328,'Ficha TÃ©cnica SA 2','Elemento 2',1500902190);
/*!40000 ALTER TABLE `elementoFichaBahia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fichaTecnicaBahia`
--

DROP TABLE IF EXISTS `fichaTecnicaBahia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fichaTecnicaBahia` (
  `idfichaTecnicaBahia` int(11) NOT NULL,
  `bahia` int(11) NOT NULL,
  PRIMARY KEY (`idfichaTecnicaBahia`),
  KEY `fk_fichaTecnicaBahia_bahia1_idx` (`bahia`),
  CONSTRAINT `fk_fichaTecnicaBahia_bahia1` FOREIGN KEY (`bahia`) REFERENCES `bahia` (`idbahia`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fichaTecnicaBahia`
--

LOCK TABLES `fichaTecnicaBahia` WRITE;
/*!40000 ALTER TABLE `fichaTecnicaBahia` DISABLE KEYS */;
INSERT INTO `fichaTecnicaBahia` VALUES (1500901147,1500901147),(1500902190,1500902190);
/*!40000 ALTER TABLE `fichaTecnicaBahia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fichaTecnicaIP`
--

DROP TABLE IF EXISTS `fichaTecnicaIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fichaTecnicaIP` (
  `fichaTecnicaIP` int(11) NOT NULL,
  `instalacionPortuaria` int(11) NOT NULL,
  PRIMARY KEY (`fichaTecnicaIP`),
  KEY `fk_fichaTecnicaIP_instalacionPortuaria1_idx` (`instalacionPortuaria`),
  CONSTRAINT `fk_fichaTecnicaIP_instalacionPortuaria1` FOREIGN KEY (`instalacionPortuaria`) REFERENCES `instalacionPortuaria` (`idinstalacionPortuaria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fichaTecnicaIP`
--

LOCK TABLES `fichaTecnicaIP` WRITE;
/*!40000 ALTER TABLE `fichaTecnicaIP` DISABLE KEYS */;
INSERT INTO `fichaTecnicaIP` VALUES (1500901486,1500901486),(1500902421,1500902421);
/*!40000 ALTER TABLE `fichaTecnicaIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fotografiaBahia`
--

DROP TABLE IF EXISTS `fotografiaBahia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fotografiaBahia` (
  `idfotografiaBahia` int(11) NOT NULL,
  `nombreFoto` varchar(45) NOT NULL,
  `descripcion` varchar(4500) NOT NULL,
  `bahia` int(11) NOT NULL,
  PRIMARY KEY (`idfotografiaBahia`),
  KEY `fk_fotografiaBahia_bahia1_idx` (`bahia`),
  CONSTRAINT `fk_fotografiaBahia_bahia1` FOREIGN KEY (`bahia`) REFERENCES `bahia` (`idbahia`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fotografiaBahia`
--

LOCK TABLES `fotografiaBahia` WRITE;
/*!40000 ALTER TABLE `fotografiaBahia` DISABLE KEYS */;
INSERT INTO `fotografiaBahia` VALUES (1500901462,'download.png','Primera Imagen de Prueba',1500901147),(1500902400,'alerta.png','imagen de prueba',1500902190);
/*!40000 ALTER TABLE `fotografiaBahia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `georeferenciaInfor`
--

DROP TABLE IF EXISTS `georeferenciaInfor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `georeferenciaInfor` (
  `idgeoreferencia` int(11) NOT NULL,
  `zona` int(11) NOT NULL,
  `latitud` varchar(45) NOT NULL,
  `longitud` varchar(45) NOT NULL,
  PRIMARY KEY (`idgeoreferencia`),
  KEY `fk_georeferenciaZE_zonaEspera1_idx` (`zona`),
  CONSTRAINT `fk_georeferenciaZE_zonaEspera1` FOREIGN KEY (`zona`) REFERENCES `zona` (`idzona`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `georeferenciaInfor`
--

LOCK TABLES `georeferenciaInfor` WRITE;
/*!40000 ALTER TABLE `georeferenciaInfor` DISABLE KEYS */;
INSERT INTO `georeferenciaInfor` VALUES (1500901916,1500901875,'-33.0393200','-71.6272500'),(1500901935,1500901846,'-33.0393200','-71.6272500'),(1500902006,1500901855,'-33.0393200','-71.6272500');
/*!40000 ALTER TABLE `georeferenciaInfor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gereferenciaArea`
--

DROP TABLE IF EXISTS `gereferenciaArea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gereferenciaArea` (
  `idgereferenciaArea` int(11) NOT NULL,
  `areaManiobra` int(11) NOT NULL,
  `latitud` varchar(45) NOT NULL,
  `longitud` varchar(45) NOT NULL,
  PRIMARY KEY (`idgereferenciaArea`),
  KEY `fk_gereferenciaArea_areaManiobra1_idx` (`areaManiobra`),
  CONSTRAINT `fk_gereferenciaArea_areaManiobra1` FOREIGN KEY (`areaManiobra`) REFERENCES `areaManiobra` (`idareaManiobra`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gereferenciaArea`
--

LOCK TABLES `gereferenciaArea` WRITE;
/*!40000 ALTER TABLE `gereferenciaArea` DISABLE KEYS */;
INSERT INTO `gereferenciaArea` VALUES (1500901806,1500901745,'-33.0393200','-71.6272500');
/*!40000 ALTER TABLE `gereferenciaArea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagenIP`
--

DROP TABLE IF EXISTS `imagenIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagenIP` (
  `idimagenIP` int(11) NOT NULL,
  `nombreImagen` varchar(450) NOT NULL,
  `descripcion` varchar(4500) NOT NULL,
  `instalacionPortuaria` int(11) NOT NULL,
  PRIMARY KEY (`idimagenIP`),
  KEY `fk_imagenIP_instalacionPortuaria1_idx` (`instalacionPortuaria`),
  CONSTRAINT `fk_imagenIP_instalacionPortuaria1` FOREIGN KEY (`instalacionPortuaria`) REFERENCES `instalacionPortuaria` (`idinstalacionPortuaria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagenIP`
--

LOCK TABLES `imagenIP` WRITE;
/*!40000 ALTER TABLE `imagenIP` DISABLE KEYS */;
INSERT INTO `imagenIP` VALUES (1500901725,'historico.png','Imagen de prueba',1500901486),(1500902600,'alerta.png','Imagen de prueba',1500902421);
/*!40000 ALTER TABLE `imagenIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `infoGeoreferenciaIP`
--

DROP TABLE IF EXISTS `infoGeoreferenciaIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `infoGeoreferenciaIP` (
  `idinfoGeoreferenciaIP` int(11) NOT NULL,
  `instalacionPortuaria` int(11) NOT NULL,
  `latitud` varchar(45) NOT NULL,
  `longitud` varchar(45) NOT NULL,
  PRIMARY KEY (`idinfoGeoreferenciaIP`),
  KEY `fk_infoGeoreferenciaIP_instalacionPortuaria1_idx` (`instalacionPortuaria`),
  CONSTRAINT `fk_infoGeoreferenciaIP_instalacionPortuaria1` FOREIGN KEY (`instalacionPortuaria`) REFERENCES `instalacionPortuaria` (`idinstalacionPortuaria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `infoGeoreferenciaIP`
--

LOCK TABLES `infoGeoreferenciaIP` WRITE;
/*!40000 ALTER TABLE `infoGeoreferenciaIP` DISABLE KEYS */;
INSERT INTO `infoGeoreferenciaIP` VALUES (1500901615,1500901486,'-33.0393200','-71.6272500'),(1500902510,1500902421,'-33.5836','-71.6131');
/*!40000 ALTER TABLE `infoGeoreferenciaIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `infoGeoreferenciacion`
--

DROP TABLE IF EXISTS `infoGeoreferenciacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `infoGeoreferenciacion` (
  `idinfoGeoreferenciacion` int(11) NOT NULL,
  `longitud` varchar(450) NOT NULL,
  `latitud` varchar(450) NOT NULL,
  `bahia` int(11) NOT NULL,
  `estadoVisible` varchar(45) NOT NULL,
  PRIMARY KEY (`idinfoGeoreferenciacion`),
  KEY `fk_infoGeoreferenciacion_bahia1_idx` (`bahia`),
  CONSTRAINT `fk_infoGeoreferenciacion_bahia1` FOREIGN KEY (`bahia`) REFERENCES `bahia` (`idbahia`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `infoGeoreferenciacion`
--

LOCK TABLES `infoGeoreferenciacion` WRITE;
/*!40000 ALTER TABLE `infoGeoreferenciacion` DISABLE KEYS */;
INSERT INTO `infoGeoreferenciacion` VALUES (1500901180,'-71.6272500','-33.0393200',1500901147,'VISIBLE'),(1500902241,'-33.5836','-71.6131',1500902190,'NO VISIBLE');
/*!40000 ALTER TABLE `infoGeoreferenciacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instalacionPortuaria`
--

DROP TABLE IF EXISTS `instalacionPortuaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instalacionPortuaria` (
  `idinstalacionPortuaria` int(11) NOT NULL,
  `nombreInstalacion` varchar(450) NOT NULL,
  `bahia` int(11) NOT NULL,
  PRIMARY KEY (`idinstalacionPortuaria`),
  KEY `fk_instalacionPortuaria_bahia1_idx` (`bahia`),
  CONSTRAINT `fk_instalacionPortuaria_bahia1` FOREIGN KEY (`bahia`) REFERENCES `bahia` (`idbahia`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instalacionPortuaria`
--

LOCK TABLES `instalacionPortuaria` WRITE;
/*!40000 ALTER TABLE `instalacionPortuaria` DISABLE KEYS */;
INSERT INTO `instalacionPortuaria` VALUES (1500901486,'InstalaciÃ³n Portuaria ValparaÃ­so',1500901147),(1500902421,'Puerto San Antonio',1500902190);
/*!40000 ALTER TABLE `instalacionPortuaria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfilUsuario`
--

DROP TABLE IF EXISTS `perfilUsuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfilUsuario` (
  `idperfilUsuario` int(11) NOT NULL,
  `nombrePerfil` varchar(45) NOT NULL,
  PRIMARY KEY (`idperfilUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfilUsuario`
--

LOCK TABLES `perfilUsuario` WRITE;
/*!40000 ALTER TABLE `perfilUsuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `perfilUsuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombreUsuario` varchar(45) NOT NULL,
  `claveUsuario` varchar(45) NOT NULL,
  `perfilUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_usuario_perfilUsuario_idx` (`perfilUsuario`),
  CONSTRAINT `fk_usuario_perfilUsuario` FOREIGN KEY (`perfilUsuario`) REFERENCES `perfilUsuario` (`idperfilUsuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zona`
--

DROP TABLE IF EXISTS `zona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zona` (
  `idzona` int(11) NOT NULL,
  `nombreZona` varchar(450) NOT NULL,
  `tipoZona` varchar(45) NOT NULL,
  `instalacionPortuaria` int(11) NOT NULL,
  PRIMARY KEY (`idzona`),
  KEY `fk_zonaEspera_instalacionPortuaria1_idx` (`instalacionPortuaria`),
  CONSTRAINT `fk_zonaEspera_instalacionPortuaria1` FOREIGN KEY (`instalacionPortuaria`) REFERENCES `instalacionPortuaria` (`idinstalacionPortuaria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zona`
--

LOCK TABLES `zona` WRITE;
/*!40000 ALTER TABLE `zona` DISABLE KEYS */;
INSERT INTO `zona` VALUES (1500901846,'Zona de Espera','ESPERA',1500901486),(1500901855,'Zona de Fondeo','FONDEO',1500901486),(1500901875,'Zona de Atraque','OTRAS',1500901486);
/*!40000 ALTER TABLE `zona` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-24 10:28:41
