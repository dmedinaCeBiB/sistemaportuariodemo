$(document).on("ready", function(){
	listar();
	eliminar();
	guardar();
	editar();

});

	var listar = function(){
		
		var dataID = getQuerystring("data");
		var table = $("#documentos").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				url: "../php/bahiaDocumento/showData.php?id="+dataID
			},
			"columns":[
				{"data":"nombreDocumento"},
				{"data":"descripcion"},						
				{"data":"nombreBahia"},		
				{"defaultContent": "<button type='button' class='caracteristica btn btn-success' ><i class='fa fa-file-o'></i></button>"},				
				{"defaultContent": "<button type='button' class='editar btn btn-primary' data-toggle='modal' data-target='#myModalEditar'><i class='fa fa-pencil-square-o'></i></button>	<button type='button' class='eliminar btn btn-danger' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-trash-o'></i></button>"}	
			],
			"language": idioma_espanol
		});
		
		obtener_id_eliminar("#documentos tbody", table);
		obtener_data_editar("#documentos tbody", table);
		redireccionarCaracteristica("#documentos tbody", table);		
	}
	
	//funcion para recuperar la clave del valor obtenido por paso de referencia
	function getQuerystring(key, default_) {
		if (default_ == null)
			default_ = "";
		key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regex = new RegExp("[\\?&amp;]"+key+"=([^&amp;#]*)");
		var qs = regex.exec(window.location.href);
		if(qs == null)
			return default_;
		else
			return qs[1];
	};
	
	/*redireccionamiento a caracteristicas*/
	var redireccionarCaracteristica = function(tbody, table){
		$(tbody).on("click", "button.caracteristica", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			var bahia = getQuerystring('data');
			location.href="../../../resource/bahia/documents/"+bahia+"/"+data.nombreDocumento;
		});
	}
				
	var obtener_id_eliminar = function(tbody, table){
		$(tbody).on("click", "button.eliminar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var iddocumentoBahia = $("#frmEliminar #iddocumentoBahia").val( data.iddocumentoBahia );
			var nameFile = $("#frmEliminar #nameFile").val( data.nombreDocumento );
		});
	}
	
	var obtener_data_editar = function(tbody, table){
		$(tbody).on("click", "button.editar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var iddocumentoBahia = $("#frmEditar #iddocumentoBahia").val(data.iddocumentoBahia);			
			var descripcion = $("#frmEditar #descripcion").val( data.descripcion );						
		});
	}
		
	var eliminar = function(){
		$("#eliminar-document").on("click", function(){
			var iddocumentoBahia = $("#frmEliminar #iddocumentoBahia").val()
			var idbahia = getQuerystring('data');
			var nameFile = $("#frmEliminar #nameFile").val();
			
			$.ajax({
				method:"POST",
				url: "../php/bahiaDocumento/removeData.php",
				data: {
						"iddocumentoBahia": iddocumentoBahia,
						"idbahia" : idbahia,
						"nameFile" : nameFile
					  }
			}).done( function( info ){
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				listar();
			});
		});
	}
	
	var guardar = function(){
		$("#agregar-documento").on("click", function(){			
			
			var dataID = getQuerystring('data');
			var name = $("#frmAgregar #name").val();						
			var desc = $("#frmAgregar #desc").val();						
				
			$.ajax({
				method: "POST",
				url: "../php/bahiaDocumento/addData.php",
				data: {
						"dataID"   	: dataID,
						"name"   	: name,
						"desc"   	: desc
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );			
				location.reload(true);
			});
		});
	}
	
	var editar = function(){
		$("#editar-documento").on("click", function(){			
			
			var iddocumentoBahia = $("#frmEditar #iddocumentoBahia").val();			
			var descripcion = $("#frmEditar #descripcion").val();						

			$.ajax({
				method: "POST",
				url: "../php/bahiaDocumento/editData.php",
				data: {
						"iddocumentoBahia"   	: iddocumentoBahia,
						"descripcion"   	: descripcion
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				listar();
				
			});
		});
	}
	
	var mostrar_mensaje = function( informacion ){
		var texto = "", color = "";
			if( informacion.respuesta == "BIEN" ){
				texto = "<strong>Bien!</strong> Se han guardado los cambios correctamente.";
				color = "#379911";
			}else if( informacion.respuesta == "ERROR"){
				texto = "<strong>Error</strong>, no se ejecutó la consulta.";
				color = "#C9302C";
			}else if( informacion.respuesta == "EXISTE" ){
				texto = "<strong>Información!</strong> el usuario ya existe.";
				color = "#5b94c5";
			}else if( informacion.respuesta == "VACIO" ){
				texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
				color = "#ddb11d";
			}else if( informacion.respuesta == "OPCION_VACIA" ){
				texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
				color = "#ddb11d";
			}

			$(".mensaje").html( texto ).css({"color": color });
			$(".mensaje").fadeOut(5000, function(){
				$(this).html("");
				$(this).fadeIn(3000);
			});			
		}
	
	var limpiar_datos = function(){			
			$("#detalle").val("");
			$("#link").val("");
			$("#titular").val("");
			
	}
	
	var limpiar_datos2 = function(){			
			$("#link2").val("");
			$("#desc2").val("");
			$("#sitio2").val("");
			
	}
			
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}


