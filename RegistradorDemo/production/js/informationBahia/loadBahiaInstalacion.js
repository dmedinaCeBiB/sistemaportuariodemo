$(document).on("ready", function(){
	listar();
	eliminar();
	guardar();
	editar();

});

	var listar = function(){
		
		var dataID = getQuerystring("data");
		
		var table = $("#instalacionPort").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				"url": "../php/bahiaInstalacion/showData.php?id="+dataID
			},
			"columns":[
				{"data":"nombreInstalacion"},
				{"data":"nombreBahia"},
				{"defaultContent": "<button type='button' class='areas btn btn-success' ><i class='fa fa-table'></i></button> <button type='button' class='zonas btn btn-success' ><i class='fa fa-th'></i></button>"},
				{"defaultContent": "<button type='button' class='fichaTecnica btn btn-success' ><i class='fa fa-calendar-check-o'></i></button> <button type='button' class='caracteristica btn btn-success' ><i class='fa fa-ship'></i></button> <button type='button' class='georeferencia btn btn-success' ><i class='fa fa-map-marker'></i></button> <button type='button' class='documentos btn btn-success' ><i class='fa fa-archive'></i></button> <button type='button' class='imagenes btn btn-success' ><i class='fa fa-camera-retro'></i></button> <button type='button' class='antecedentes btn btn-success' ><i class='fa fa-language'></i></button>"},								
				{"defaultContent": "<button type='button' class='editar btn btn-primary' data-toggle='modal' data-target='#myModalEditar'><i class='fa fa-pencil-square-o'></i></button>	<button type='button' class='eliminar btn btn-danger' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-trash-o'></i></button>"}	
			],
			"language": idioma_espanol
		});
		
		obtener_id_eliminar("#instalacionPort tbody", table);
		obtener_data_editar("#instalacionPort tbody", table);
		
		/*redireccionamiento*/
		redireccionarArea("#instalacionPort tbody", table);		
		redireccionarZona("#instalacionPort tbody", table);		
		redireccionarFichaTec("#instalacionPort tbody", table);		
		redireccionarCaracteristica("#instalacionPort tbody", table);		
		redireccionarInfoGeo("#instalacionPort tbody", table);		
		redireccionarDocumentos("#instalacionPort tbody", table);		
		redireccionarGaleria("#instalacionPort tbody", table);		
		redireccionarAntecedentes("#instalacionPort tbody", table);		
	}
	
	/*redireccionamiento a areas*/
	var redireccionarArea = function(tbody, table){
		$(tbody).on("click", "button.areas", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../instalacionPortuariaBahia/area/index.html?data="+data.idinstalacionPortuaria;
		});
	}
		
	/*redireccionamiento a zonas*/
	var redireccionarZona = function(tbody, table){
		$(tbody).on("click", "button.zonas", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../instalacionPortuariaBahia/zona/index.html?data="+data.idinstalacionPortuaria;
		});
	}

	/*redireccionamiento a fichaTecnica*/
	var redireccionarFichaTec = function(tbody, table){
		$(tbody).on("click", "button.fichaTecnica", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../instalacionPortuariaBahia/fichaTecnica/index.html?data="+data.idinstalacionPortuaria;
		});
	}

	/*redireccionamiento a caracteristicas*/
	var redireccionarCaracteristica = function(tbody, table){
		$(tbody).on("click", "button.caracteristica", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../instalacionPortuariaBahia/caracteristicas/index.html?data="+data.idinstalacionPortuaria;
		});
	}

	/*redireccionamiento a georeferenciacion*/
	var redireccionarInfoGeo = function(tbody, table){
		$(tbody).on("click", "button.georeferencia", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../instalacionPortuariaBahia/georeferencia/index.html?data="+data.idinstalacionPortuaria;
		});
	}
	
	/*redireccionamiento a archivos*/
	var redireccionarDocumentos = function(tbody, table){
		$(tbody).on("click", "button.documentos", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../instalacionPortuariaBahia/archivos/index.html?data="+data.idinstalacionPortuaria;
		});
	}
	
	/*redireccionamiento a imagenes*/
	var redireccionarGaleria = function(tbody, table){
		$(tbody).on("click", "button.imagenes", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../instalacionPortuariaBahia/imagenes/index.html?data="+data.idinstalacionPortuaria;
		});
	}
	
	/*redireccionamiento a antecedentes*/
	var redireccionarAntecedentes = function(tbody, table){
		$(tbody).on("click", "button.antecedentes", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../instalacionPortuariaBahia/antecedentes/index.html?data="+data.idinstalacionPortuaria;
		});
	}
	
	var obtener_id_eliminar = function(tbody, table){
		$(tbody).on("click", "button.eliminar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var idinstalacionPortuaria = $("#frmEliminar #idinstalacionPortuaria").val( data.idinstalacionPortuaria );
		});
	}
	
	var obtener_data_editar = function(tbody, table){
		$(tbody).on("click", "button.editar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var idinstalacionPortuaria = $("#frmEditar #idinstalacionPortuaria").val(data.idinstalacionPortuaria);			
			var nameInstalacion = $("#frmEditar #nameInstalacion").val( data.nombreInstalacion );			
			
		});
	}
		
	var eliminar = function(){
		$("#eliminar-instalacion").on("click", function(){
			var idinstalacionPortuaria = $("#frmEliminar #idinstalacionPortuaria").val()
			$.ajax({
				method:"POST",
				url: "../php/bahiaInstalacion/removeData.php",
				data: {
						"idinstalacionPortuaria": idinstalacionPortuaria
					  }
			}).done( function( info ){
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				location.reload(true);
			});
		});
	}
	
	var guardar = function(){
		$("#agregar-instalacion").on("click", function(){			
			
			var dataID = getQuerystring('data');
			var name = $("#frmAgregar #name").val();						
				
			$.ajax({
				method: "POST",
				url: "../php/bahiaInstalacion/addData.php",
				data: {
						"name"   	: name,
						"dataID"   	: dataID						
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );			
				location.reload(true);
			});
		});
	}
	
	var editar = function(){
		$("#editar-instalacion").on("click", function(){			
			
			var idinstalacionPortuaria = $("#frmEditar #idinstalacionPortuaria").val();			
			var nameInstalacion = $("#frmEditar #nameInstalacion").val();			

			$.ajax({
				method: "POST",
				url: "../php/bahiaInstalacion/editData.php",
				data: {
						"idinstalacionPortuaria"   	: idinstalacionPortuaria,
						"nameInstalacion" 	 		: nameInstalacion
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );				
				listar();
			});
		});
	}
	
	//funcion para recuperar la clave del valor obtenido por paso de referencia
	function getQuerystring(key, default_) {
		if (default_ == null)
			default_ = "";
		key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regex = new RegExp("[\\?&amp;]"+key+"=([^&amp;#]*)");
		var qs = regex.exec(window.location.href);
		if(qs == null)
			return default_;
		else
			return qs[1];
	};
	
	var mostrar_mensaje = function( informacion ){
		var texto = "", color = "";
			if( informacion.respuesta == "BIEN" ){
				texto = "<strong>Bien!</strong> Se han guardado los cambios correctamente.";
				color = "#379911";
			}else if( informacion.respuesta == "ERROR"){
				texto = "<strong>Error</strong>, no se ejecutó la consulta.";
				color = "#C9302C";
			}else if( informacion.respuesta == "EXISTE" ){
				texto = "<strong>Información!</strong> el usuario ya existe.";
				color = "#5b94c5";
			}else if( informacion.respuesta == "VACIO" ){
				texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
				color = "#ddb11d";
			}else if( informacion.respuesta == "OPCION_VACIA" ){
				texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
				color = "#ddb11d";
			}

			$(".mensaje").html( texto ).css({"color": color });
			$(".mensaje").fadeOut(5000, function(){
				$(this).html("");
				$(this).fadeIn(3000);
			});			
		}
	
	var limpiar_datos = function(){			
			$("#detalle").val("");
			$("#link").val("");
			$("#titular").val("");
			
	}
	
	var limpiar_datos2 = function(){			
			$("#link2").val("");
			$("#desc2").val("");
			$("#sitio2").val("");
			
	}
			
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}

