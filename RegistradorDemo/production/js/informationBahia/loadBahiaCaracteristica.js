$(document).on("ready", function(){
	listar();
	eliminar();
	guardar();
	editar();

});

	var listar = function(){
		
		var dataID = getQuerystring("data");
		var table = $("#caracteristicas").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				url: "../php/bahiaCaracteristicas/showData.php?id="+dataID
			},
			"columns":[
				{"data":"nombreCaracteristica"},
				{"data":"valorCaracteristica"},		
				{"data":"nombreBahia"},		
				{"defaultContent": "<button type='button' class='editar btn btn-primary' data-toggle='modal' data-target='#myModalEditar'><i class='fa fa-pencil-square-o'></i></button>	<button type='button' class='eliminar btn btn-danger' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-trash-o'></i></button>"}	
			],
			"language": idioma_espanol
		});
		
		obtener_id_eliminar("#caracteristicas tbody", table);
		obtener_data_editar("#caracteristicas tbody", table);
	}
	
	//funcion para recuperar la clave del valor obtenido por paso de referencia
	function getQuerystring(key, default_) {
		if (default_ == null)
			default_ = "";
		key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regex = new RegExp("[\\?&amp;]"+key+"=([^&amp;#]*)");
		var qs = regex.exec(window.location.href);
		if(qs == null)
			return default_;
		else
			return qs[1];
	};
			
	var obtener_id_eliminar = function(tbody, table){
		$(tbody).on("click", "button.eliminar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var idcaracteristicaBahia = $("#frmEliminar #idcaracteristicaBahia").val( data.idcaracteristicaBahia );
		});
	}
	
	var obtener_data_editar = function(tbody, table){
		$(tbody).on("click", "button.editar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var nombre = $("#frmEditar #name").val(data.nombreCaracteristica);			
			var detalle = $("#frmEditar #valor").val( data.valorCaracteristica );			
			var idcaracteristicaBahia = $("#frmEditar #idcaracteristicaBahia").val(data.idcaracteristicaBahia);			
		});
	}
		
	var eliminar = function(){
		$("#eliminar-caracteristica").on("click", function(){
			var idcaracteristicaBahia = $("#frmEliminar #idcaracteristicaBahia").val()
			$.ajax({
				method:"POST",
				url: "../php/bahiaCaracteristicas/removeData.php",
				data: {
						"idcaracteristicaBahia": idcaracteristicaBahia
					  }
			}).done( function( info ){
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				listar();
			});
		});
	}
	
	var guardar = function(){
		$("#agregar-característica").on("click", function(){			
			
			var dataID = getQuerystring('data');
			var name = $("#frmAgregar #name").val();						
			var valor = $("#frmAgregar #valor").val();						
				
			$.ajax({
				method: "POST",
				url: "../php/bahiaCaracteristicas/addData.php",
				data: {
						"dataID"   	: dataID,
						"name"   	: name,
						"valor"   	: valor
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );			
				location.reload(true);
			});
		});
	}
	
	var editar = function(){
		$("#editar-caracteristica").on("click", function(){			
			
			var name = $("#frmEditar #name").val();			
			var valor = $("#frmEditar #valor").val();			
			var idcaracteristicaBahia = $("#frmEditar #idcaracteristicaBahia").val();			

			$.ajax({
				method: "POST",
				url: "../php/bahiaCaracteristicas/editData.php",
				data: {
						"name"   	: name,
						"valor"   	: valor,
						"idcaracteristicaBahia"   	: idcaracteristicaBahia
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				listar();
				
			});
		});
	}
	
	var mostrar_mensaje = function( informacion ){
		var texto = "", color = "";
			if( informacion.respuesta == "BIEN" ){
				texto = "<strong>Bien!</strong> Se han guardado los cambios correctamente.";
				color = "#379911";
			}else if( informacion.respuesta == "ERROR"){
				texto = "<strong>Error</strong>, no se ejecutó la consulta.";
				color = "#C9302C";
			}else if( informacion.respuesta == "EXISTE" ){
				texto = "<strong>Información!</strong> el usuario ya existe.";
				color = "#5b94c5";
			}else if( informacion.respuesta == "VACIO" ){
				texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
				color = "#ddb11d";
			}else if( informacion.respuesta == "OPCION_VACIA" ){
				texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
				color = "#ddb11d";
			}

			$(".mensaje").html( texto ).css({"color": color });
			$(".mensaje").fadeOut(5000, function(){
				$(this).html("");
				$(this).fadeIn(3000);
			});			
		}
	
	var limpiar_datos = function(){			
			$("#detalle").val("");
			$("#link").val("");
			$("#titular").val("");
			
	}
	
	var limpiar_datos2 = function(){			
			$("#link2").val("");
			$("#desc2").val("");
			$("#sitio2").val("");
			
	}
			
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}


