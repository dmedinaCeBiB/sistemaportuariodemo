$(document).on("ready", function(){
	listar();
	eliminar();
	guardar();
	editar();

});

	var listar = function(){
	
		var table = $("#bahiaCostera").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				"url": "../php/bahia/showData.php"
			},
			"columns":[
				{"data":"nombreBahia"},
				{"defaultContent": "<button type='button' class='caracteristica btn btn-success' ><i class='fa fa-ship'></i></button>"},				
				{"defaultContent": "<button type='button' class='georeference btn btn-success' ><i class='fa fa-map-marker'></i></button>"},
				{"defaultContent": "<button type='button' class='fichaTecnica btn btn-success' ><i class='fa fa-calendar-check-o'></i></button>"},
				{"defaultContent": "<button type='button' class='documentos btn btn-success' ><i class='fa fa-archive'></i></button>"},
				{"defaultContent": "<button type='button' class='galeria btn btn-success' ><i class='fa fa-camera-retro'></i></button>"},
				{"defaultContent": "<button type='button' class='instalaciones btn btn-success' ><i class='fa fa-anchor'></i></button>"},
				{"defaultContent": "<button type='button' class='editar btn btn-primary' data-toggle='modal' data-target='#myModalEditar'><i class='fa fa-pencil-square-o'></i></button>	<button type='button' class='eliminar btn btn-danger' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-trash-o'></i></button>"}	
			],
			"language": idioma_espanol
		});
		
		obtener_id_eliminar("#bahiaCostera tbody", table);
		obtener_data_editar("#bahiaCostera tbody", table);
		
		/*redireccionamiento*/
		redireccionarCaracteristica("#bahiaCostera tbody", table);		
		redireccionarInfoGeo("#bahiaCostera tbody", table);		
		redireccionarFichaTec("#bahiaCostera tbody", table);		
		redireccionarDocumentos("#bahiaCostera tbody", table);		
		redireccionarGaleria("#bahiaCostera tbody", table);		
		redireccionarInstalacion("#bahiaCostera tbody", table);		
	}
	
	/*redireccionamiento a caracteristicas*/
	var redireccionarCaracteristica = function(tbody, table){
		$(tbody).on("click", "button.caracteristica", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../bahiaCaracteristica/index.html?data="+data.idbahia;
		});
	}
	
	/*redireccionamiento a informacion geografica*/
	var redireccionarInfoGeo = function(tbody, table){
		$(tbody).on("click", "button.georeference", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../bahiaGeo/index.html?data="+data.idbahia;
		});
	}
	
	/*redireccionamiento a ficha tecnica*/
	var redireccionarFichaTec = function(tbody, table){
		$(tbody).on("click", "button.fichaTecnica", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../bahiaFicha/index.html?data="+data.idbahia;
		});
	}
	
	/*redireccionamiento a documentos*/
	var redireccionarDocumentos = function(tbody, table){
		$(tbody).on("click", "button.documentos", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../bahiaDocumento/index.html?data="+data.idbahia;
		});
	}
	
	/*redireccionamiento a galeria*/
	var redireccionarGaleria = function(tbody, table){
		$(tbody).on("click", "button.galeria", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../bahiaGaleria/index.html?data="+data.idbahia;
		});
	}
	
	/*redireccionamiento a instalaciones*/
	var redireccionarInstalacion = function(tbody, table){
		$(tbody).on("click", "button.instalaciones", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../bahiaInstalacion/index.html?data="+data.idbahia;
		});
	}
	
	var obtener_id_eliminar = function(tbody, table){
		$(tbody).on("click", "button.eliminar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var idbibliografia = $("#frmEliminar #idbahia").val( data.idbahia );
		});
	}
	
	var obtener_data_editar = function(tbody, table){
		$(tbody).on("click", "button.editar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var idbib = $("#frmEditar #idbahia").val(data.idbahia);			
			var name = $("#frmEditar #bahiaName").val( data.nombreBahia );			
			
		});
	}
		
	var eliminar = function(){
		$("#eliminar-bahia").on("click", function(){
			var idbahia = $("#frmEliminar #idbahia").val()
			$.ajax({
				method:"POST",
				url: "../php/bahia/removeData.php",
				data: {
						"idbahia": idbahia
					  }
			}).done( function( info ){
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				location.reload(true);
			});
		});
	}
	
	var guardar = function(){
		$("#agregar-bahia").on("click", function(){			
			
			var name = $("#frmAgregar #bahiaName").val();						
				
			$.ajax({
				method: "POST",
				url: "../php/bahia/addData.php",
				data: {
						"name"   	: name						
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );			
				location.reload(true);
			});
		});
	}
	
	var editar = function(){
		$("#editar-bahia").on("click", function(){			
			
			var idbahia = $("#frmEditar #idbahia").val();			
			var name = $("#frmEditar #bahiaName").val();			

			$.ajax({
				method: "POST",
				url: "../php/bahia/editData.php",
				data: {
						"idbahia"   	: idbahia,
						"name" 	 	: name
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );				
				listar();
			});
		});
	}
	
	var mostrar_mensaje = function( informacion ){
		var texto = "", color = "";
			if( informacion.respuesta == "BIEN" ){
				texto = "<strong>Bien!</strong> Se han guardado los cambios correctamente.";
				color = "#379911";
			}else if( informacion.respuesta == "ERROR"){
				texto = "<strong>Error</strong>, no se ejecutó la consulta.";
				color = "#C9302C";
			}else if( informacion.respuesta == "EXISTE" ){
				texto = "<strong>Información!</strong> el usuario ya existe.";
				color = "#5b94c5";
			}else if( informacion.respuesta == "VACIO" ){
				texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
				color = "#ddb11d";
			}else if( informacion.respuesta == "OPCION_VACIA" ){
				texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
				color = "#ddb11d";
			}

			$(".mensaje").html( texto ).css({"color": color });
			$(".mensaje").fadeOut(5000, function(){
				$(this).html("");
				$(this).fadeIn(3000);
			});			
		}
	
	var limpiar_datos = function(){			
			$("#detalle").val("");
			$("#link").val("");
			$("#titular").val("");
			
	}
	
	var limpiar_datos2 = function(){			
			$("#link2").val("");
			$("#desc2").val("");
			$("#sitio2").val("");
			
	}
			
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}

