<?php
	
	include ("../connection.php");
	
	#hacemos la obtencion de los datos
	$name = $_REQUEST['name'];
	
	#obtenemos el valor del id de la bahia (sera un entero en base al tiempo)
	$dataID = time();
	
	#hacemos la consulta
	$query = "INSERT INTO  bahia values ($dataID, '$name')";
	$dataIDFicha = time();
	$queryFicha = "INSERT INTO fichaTecnicaBahia values ($dataIDFicha, $dataID)";
	$resultado = mysqli_query($conexion, $query);
	$resultadoFT = mysqli_query($conexion, $queryFicha);
	verificar_resultado( $resultado, $dataID);
	cerrar( $conexion );
	
	function verificar_resultado($resultado, $dataID){
		
		if (!$resultado){
			$informacion["respuesta"] = "ERROR";
			
		}else{
			$informacion["respuesta"] ="BIEN";
			//creamos el directorio con el id de la bahia tanto en los documentos como en las imagenes
			
			$carpetaDocument = '../../../../resource/bahia/documents/'.$dataID;
			$carpetaImages = '../../../../resource/bahia/images/'.$dataID;
			if (!file_exists($carpetaDocument)) {
				mkdir($carpetaDocument, 0777, true);
			}
			
			if (!file_exists($carpetaImages)) {
				mkdir($carpetaImages, 0777, true);
			}			

		}
		echo json_encode($informacion);
	}
	
	function cerrar($conexion){
		mysqli_close($conexion);
	}

?>
