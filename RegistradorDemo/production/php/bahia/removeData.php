<?php
	
	include ("../connection.php");

	#hacemos la consulta
	$idbahia = $_REQUEST['idbahia'];
	
	$query = "delete from bahia where bahia.idbahia = $idbahia";
	$resultado = mysqli_query($conexion, $query);
	verificar_resultado( $resultado, $idbahia);
	cerrar( $conexion );
	
	
	function verificar_resultado($resultado, $idbahia){
		
		if (!$resultado){
			 $informacion["respuesta"] = "ERROR";
		
		}else{ 
			$informacion["respuesta"] ="BIEN";
			
			//borramos la carpeta con todo el contenido existente...
			$carpetaDocument = '../../../../resource/bahia/documents/'.$idbahia;
			$carpetaImages = '../../../../resource/bahia/images/'.$idbahia;		
			
			eliminarDir($carpetaDocument);
			eliminarDir($carpetaImages);
		}
			
		echo json_encode($informacion);
	}
	
	function cerrar($conexion){
		mysqli_close($conexion);
	}
	
	function eliminarDir($carpeta){
		foreach(glob($carpeta . "/*") as $archivos_carpeta){
			echo $archivos_carpeta;
			if (is_dir($archivos_carpeta)){
				eliminarDir($archivos_carpeta);
			}else{
				unlink($archivos_carpeta);
			}
		}
		rmdir($carpeta);
	}
	
?>
