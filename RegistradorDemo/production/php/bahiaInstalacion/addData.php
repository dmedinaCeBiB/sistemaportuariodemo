<?php
	
	include ("../connection.php");
	
	#hacemos la obtencion de los datos
	$name = $_REQUEST['name'];
	$dataID = $_REQUEST['dataID'];	
	
	#obtenemos el valor del id de la bahia (sera un entero en base al tiempo)
	$idInstalacion = time();
	
	#hacemos la consulta
	$query = "INSERT INTO  instalacionPortuaria values ($idInstalacion, '$name', $dataID)";
	$dataIDFicha = time();
	$queryFicha = "INSERT INTO fichaTecnicaIP values ($dataIDFicha, $idInstalacion)";
	$resultado = mysqli_query($conexion, $query);
	$resultadoFT = mysqli_query($conexion, $queryFicha);
	verificar_resultado( $resultado, $idInstalacion);
	cerrar( $conexion );
	
	function verificar_resultado($resultado, $idInstalacion){
		
		if (!$resultado){
			$informacion["respuesta"] = "ERROR";
			
		}else{
			$informacion["respuesta"] ="BIEN";
			//creamos el directorio con el id de la bahia tanto en los documentos como en las imagenes
			
			$carpetaDocument = '../../../../resource/instalacion/documents/'.$idInstalacion;
			$carpetaImages = '../../../../resource/instalacion/images/'.$idInstalacion;
			if (!file_exists($carpetaDocument)) {
				mkdir($carpetaDocument, 0777, true);
			}
			
			if (!file_exists($carpetaImages)) {
				mkdir($carpetaImages, 0777, true);
			}			

		}
		echo json_encode($informacion);
	}
	
	function cerrar($conexion){
		mysqli_close($conexion);
	}

?>
