<?php
	
	include ("../connection.php");

	#hacemos la consulta
	$idinstalacionPortuaria = $_REQUEST['idinstalacionPortuaria'];
	
	$query = "delete from instalacionPortuaria where instalacionPortuaria.idinstalacionPortuaria = $idinstalacionPortuaria";
	$resultado = mysqli_query($conexion, $query);
	verificar_resultado( $resultado, $idinstalacionPortuaria);
	cerrar( $conexion );
	
	
	function verificar_resultado($resultado, $idinstalacionPortuaria){
		
		if (!$resultado){
			 $informacion["respuesta"] = "ERROR";
		
		}else{ 
			$informacion["respuesta"] ="BIEN";
			
			//borramos la carpeta con todo el contenido existente...
			$carpetaDocument = '../../../../resource/instalacion/documents/'.$idinstalacionPortuaria;
			$carpetaImages = '../../../../resource/instalacion/images/'.$idinstalacionPortuaria;		
			
			eliminarDir($carpetaDocument);
			eliminarDir($carpetaImages);
		}
			
		echo json_encode($informacion);
	}
	
	function cerrar($conexion){
		mysqli_close($conexion);
	}
	
	function eliminarDir($carpeta){
		foreach(glob($carpeta . "/*") as $archivos_carpeta){
			echo $archivos_carpeta;
			if (is_dir($archivos_carpeta)){
				eliminarDir($archivos_carpeta);
			}else{
				unlink($archivos_carpeta);
			}
		}
		rmdir($carpeta);
	}
	
?>
