-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: labSystem
-- ------------------------------------------------------
-- Server version	5.5.54-0+deb7u2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bibliografia`
--

DROP TABLE IF EXISTS `bibliografia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bibliografia` (
  `idbibliografia` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `resumen` varchar(450) NOT NULL,
  `linkInteres` varchar(45) NOT NULL,
  PRIMARY KEY (`idbibliografia`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bibliografia`
--

LOCK TABLES `bibliografia` WRITE;
/*!40000 ALTER TABLE `bibliografia` DISABLE KEYS */;
INSERT INTO `bibliografia` VALUES (1,'1','1','1'),(2,'1','w','1'),(3,'2','e','2');
/*!40000 ALTER TABLE `bibliografia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comentario`
--

DROP TABLE IF EXISTS `comentario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentario` (
  `idcomentario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(5000) NOT NULL,
  `visitante` int(11) NOT NULL,
  `estadoComentario` varchar(45) NOT NULL,
  `fecha` varchar(45) NOT NULL,
  PRIMARY KEY (`idcomentario`),
  KEY `fk_comentario_visitante1_idx` (`visitante`),
  CONSTRAINT `fk_comentario_visitante1` FOREIGN KEY (`visitante`) REFERENCES `visitante` (`idvisitante`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentario`
--

LOCK TABLES `comentario` WRITE;
/*!40000 ALTER TABLE `comentario` DISABLE KEYS */;
INSERT INTO `comentario` VALUES (4,'c','c',2,'ACEPTADO','hoy'),(5,'d','d',2,'ACEPTADO','hoy'),(6,'e','e',2,'ACEPTADO','hoy'),(7,'f','f',2,'ACEPTADO','hoy'),(8,'e','e',2,'ACEPTADO','hoy'),(9,'f','f',2,'ACEPTADO','hoy'),(10,'d','d',2,'ACEPTADO','hoy'),(12,'b','b',1,'ACEPTADO','hoy'),(13,'c','c',1,'ACEPTADO','hoy'),(14,'g','g',1,'ACEPTADO','hoy'),(15,'h','h',1,'ACEPTADO','hoy'),(16,'i','i',1,'ACEPTADO','hoy');
/*!40000 ALTER TABLE `comentario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento`
--

DROP TABLE IF EXISTS `documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento` (
  `iddocumento` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `path` varchar(100) NOT NULL,
  `proyecto` int(11) NOT NULL,
  PRIMARY KEY (`iddocumento`),
  KEY `fk_documento_proyecto1_idx` (`proyecto`),
  CONSTRAINT `fk_documento_proyecto1` FOREIGN KEY (`proyecto`) REFERENCES `proyecto` (`idproyecto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento`
--

LOCK TABLES `documento` WRITE;
/*!40000 ALTER TABLE `documento` DISABLE KEYS */;
/*!40000 ALTER TABLE `documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foto`
--

DROP TABLE IF EXISTS `foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foto` (
  `idfoto` int(11) NOT NULL AUTO_INCREMENT,
  `nombreFoto` varchar(45) NOT NULL,
  PRIMARY KEY (`idfoto`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foto`
--

LOCK TABLES `foto` WRITE;
/*!40000 ALTER TABLE `foto` DISABLE KEYS */;
INSERT INTO `foto` VALUES (2,'user.png');
/*!40000 ALTER TABLE `foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fraseLaboratoro`
--

DROP TABLE IF EXISTS `fraseLaboratoro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fraseLaboratoro` (
  `idfraseLaboratoro` int(11) NOT NULL AUTO_INCREMENT,
  `frase` varchar(500) NOT NULL,
  `estado` varchar(45) NOT NULL,
  PRIMARY KEY (`idfraseLaboratoro`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fraseLaboratoro`
--

LOCK TABLES `fraseLaboratoro` WRITE;
/*!40000 ALTER TABLE `fraseLaboratoro` DISABLE KEYS */;
INSERT INTO `fraseLaboratoro` VALUES (4,'Mi Frase','ACTIVADA');
/*!40000 ALTER TABLE `fraseLaboratoro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `herramienta`
--

DROP TABLE IF EXISTS `herramienta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `herramienta` (
  `idherramienta` int(11) NOT NULL AUTO_INCREMENT,
  `nombreHerramienta` varchar(45) NOT NULL,
  `descripcion` varchar(450) NOT NULL,
  PRIMARY KEY (`idherramienta`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `herramienta`
--

LOCK TABLES `herramienta` WRITE;
/*!40000 ALTER TABLE `herramienta` DISABLE KEYS */;
INSERT INTO `herramienta` VALUES (3,'MMOST','Algoritmo de predicciÃ³n de mutaciones cancerÃ­genas');
/*!40000 ALTER TABLE `herramienta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `infoContacto`
--

DROP TABLE IF EXISTS `infoContacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `infoContacto` (
  `idinfoContacto` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` varchar(45) NOT NULL,
  `telefonoFijo` varchar(45) NOT NULL,
  `celularContacto` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `estado` varchar(45) NOT NULL,
  PRIMARY KEY (`idinfoContacto`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `infoContacto`
--

LOCK TABLES `infoContacto` WRITE;
/*!40000 ALTER TABLE `infoContacto` DISABLE KEYS */;
INSERT INTO `infoContacto` VALUES (2,'direcciÃ³n','1234','12345432','correoporingresar@gmail.com','ACTIVADO');
/*!40000 ALTER TABLE `infoContacto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticia`
--

DROP TABLE IF EXISTS `noticia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticia` (
  `idnoticia` int(11) NOT NULL AUTO_INCREMENT,
  `titular` varchar(500) NOT NULL,
  `detalle` varchar(5000) NOT NULL,
  `link` varchar(45) NOT NULL,
  `estado` varchar(45) NOT NULL,
  PRIMARY KEY (`idnoticia`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticia`
--

LOCK TABLES `noticia` WRITE;
/*!40000 ALTER TABLE `noticia` DISABLE KEYS */;
INSERT INTO `noticia` VALUES (2,'2','2','2','ACTIVADA');
/*!40000 ALTER TABLE `noticia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participante`
--

DROP TABLE IF EXISTS `participante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participante` (
  `idParticipante` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `carrera` varchar(45) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `contacto` varchar(45) NOT NULL,
  `foto` int(11) NOT NULL,
  `nombreFoto` varchar(45) NOT NULL,
  PRIMARY KEY (`idParticipante`),
  KEY `fk_participante_foto_idx` (`foto`),
  CONSTRAINT `fk_participante_foto` FOREIGN KEY (`foto`) REFERENCES `foto` (`idfoto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participante`
--

LOCK TABLES `participante` WRITE;
/*!40000 ALTER TABLE `participante` DISABLE KEYS */;
INSERT INTO `participante` VALUES (4,'seve editada','seve editada','seve','seve','seve',2,'seve.jpg'),(6,'David Medina','d.medina@imserltda.com','IngenierÃ­a en BioinformÃ¡tica','Desarrollador de Software y scripts','+569 50966879',2,'perrito.jpg');
/*!40000 ALTER TABLE `participante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participanteProyecto`
--

DROP TABLE IF EXISTS `participanteProyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participanteProyecto` (
  `proyecto` int(11) NOT NULL,
  `participante` int(11) NOT NULL,
  `contribucion` int(11) NOT NULL,
  PRIMARY KEY (`proyecto`,`participante`),
  KEY `fk_proyecto_has_participante_participante1_idx` (`participante`),
  KEY `fk_proyecto_has_participante_proyecto1_idx` (`proyecto`),
  CONSTRAINT `fk_proyecto_has_participante_participante1` FOREIGN KEY (`participante`) REFERENCES `participante` (`idParticipante`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_proyecto_has_participante_proyecto1` FOREIGN KEY (`proyecto`) REFERENCES `proyecto` (`idproyecto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participanteProyecto`
--

LOCK TABLES `participanteProyecto` WRITE;
/*!40000 ALTER TABLE `participanteProyecto` DISABLE KEYS */;
INSERT INTO `participanteProyecto` VALUES (2,4,90),(2,6,10);
/*!40000 ALTER TABLE `participanteProyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portafolio`
--

DROP TABLE IF EXISTS `portafolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portafolio` (
  `idportafolio` int(11) NOT NULL AUTO_INCREMENT,
  `resumen` varchar(500) NOT NULL,
  `detalle` varchar(5000) NOT NULL,
  `foto` int(11) NOT NULL,
  PRIMARY KEY (`idportafolio`),
  KEY `fk_portafolio_foto1_idx` (`foto`),
  CONSTRAINT `fk_portafolio_foto1` FOREIGN KEY (`foto`) REFERENCES `foto` (`idfoto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portafolio`
--

LOCK TABLES `portafolio` WRITE;
/*!40000 ALTER TABLE `portafolio` DISABLE KEYS */;
/*!40000 ALTER TABLE `portafolio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyecto`
--

DROP TABLE IF EXISTS `proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyecto` (
  `idproyecto` int(11) NOT NULL AUTO_INCREMENT,
  `nombreProyecto` varchar(45) NOT NULL,
  `resumen` varchar(5000) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `avance` int(11) NOT NULL,
  `nombreFoto` varchar(100) NOT NULL,
  `responsable` varchar(45) NOT NULL,
  PRIMARY KEY (`idproyecto`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyecto`
--

LOCK TABLES `proyecto` WRITE;
/*!40000 ALTER TABLE `proyecto` DISABLE KEYS */;
INSERT INTO `proyecto` VALUES (2,'proyecto 01','resumen proyecto 01','ACTIVO',90,'perrito.jpg','responsable proyecto'),(3,'Proyecto Nuevo','Resumen de proyecto que es una prueba','ACTIVO',0,'1491939043_tools.png','Responsable Nuevo');
/*!40000 ALTER TABLE `proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publicacion`
--

DROP TABLE IF EXISTS `publicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publicacion` (
  `idpublicacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombrePublicacion` varchar(100) NOT NULL,
  `resumen` varchar(5000) NOT NULL,
  `linkInteres` varchar(500) NOT NULL,
  PRIMARY KEY (`idpublicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publicacion`
--

LOCK TABLES `publicacion` WRITE;
/*!40000 ALTER TABLE `publicacion` DISABLE KEYS */;
INSERT INTO `publicacion` VALUES (4,'name','desc','www');
/*!40000 ALTER TABLE `publicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumen`
--

DROP TABLE IF EXISTS `resumen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumen` (
  `idresumen` int(11) NOT NULL AUTO_INCREMENT,
  `fraseResumenP1` varchar(5000) NOT NULL,
  `fraseResumenP2` varchar(5000) NOT NULL,
  `estado` varchar(45) NOT NULL,
  PRIMARY KEY (`idresumen`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumen`
--

LOCK TABLES `resumen` WRITE;
/*!40000 ALTER TABLE `resumen` DISABLE KEYS */;
INSERT INTO `resumen` VALUES (5,'Frase Segunda','Frase nueva','ACTIVADO');
/*!40000 ALTER TABLE `resumen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seccion`
--

DROP TABLE IF EXISTS `seccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seccion` (
  `idseccion` int(11) NOT NULL AUTO_INCREMENT,
  `nombreSeccion` varchar(45) NOT NULL,
  `resumen` varchar(450) NOT NULL,
  `estado` varchar(45) NOT NULL,
  PRIMARY KEY (`idseccion`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seccion`
--

LOCK TABLES `seccion` WRITE;
/*!40000 ALTER TABLE `seccion` DISABLE KEYS */;
INSERT INTO `seccion` VALUES (3,'SecciÃ³n t','DescripciÃ³n secciÃ³n 1','ACTIVADA');
/*!40000 ALTER TABLE `seccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitioInteres`
--

DROP TABLE IF EXISTS `sitioInteres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitioInteres` (
  `idsitioInteres` int(11) NOT NULL AUTO_INCREMENT,
  `nombreLink` varchar(45) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `link` varchar(45) NOT NULL,
  `visible` varchar(45) NOT NULL,
  PRIMARY KEY (`idsitioInteres`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitioInteres`
--

LOCK TABLES `sitioInteres` WRITE;
/*!40000 ALTER TABLE `sitioInteres` DISABLE KEYS */;
INSERT INTO `sitioInteres` VALUES (6,'1','1','1','ACTIVADO');
/*!40000 ALTER TABLE `sitioInteres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitante`
--

DROP TABLE IF EXISTS `visitante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitante` (
  `idvisitante` int(11) NOT NULL AUTO_INCREMENT,
  `nombreVisitante` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `estadoVisitante` varchar(45) NOT NULL,
  PRIMARY KEY (`idvisitante`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitante`
--

LOCK TABLES `visitante` WRITE;
/*!40000 ALTER TABLE `visitante` DISABLE KEYS */;
INSERT INTO `visitante` VALUES (1,'Visitante 1','correoVisitante1','OK'),(2,'Visitante 2','correoVisitante2','OK');
/*!40000 ALTER TABLE `visitante` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-29 10:14:35
