$(document).on("ready", function(){
	listar();
	eliminar();
	guardar();
	editar();

});

	var listar = function(){
		
		var valor_llave= getQuerystring('dataSeccion');
		var table = $("#preguntas").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				"url": "../../sourcePHP/formularios/preguntas/showPregunta.php?idSeccion="+valor_llave
			},
			"columns":[
				{"data":"nombrePregunta"},
				{"data":"descripcion"},
				{"data":"puntaje"},
				{"defaultContent": "<button type='button' class='editar btn btn-primary' data-toggle='modal' data-target='#myModalEditar'><i class='fa fa-pencil-square-o'></i></button>	<button type='button' class='eliminar btn btn-danger' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-trash-o'></i></button>"}	
			],
			"language": idioma_espanol
		});
		
		obtener_id_eliminar("#preguntas tbody", table);
		obtener_data_editar("#preguntas tbody", table);
	}
	
	//funcion para recuperar la clave del valor obtenido por paso de referencia
	function getQuerystring(key, default_) {
		if (default_ == null)
			default_ = "";
		key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regex = new RegExp("[\\?&amp;]"+key+"=([^&amp;#]*)");
		var qs = regex.exec(window.location.href);
		if(qs == null)
			return default_;
		else
			return qs[1];
	};
	 
	var obtener_id_eliminar = function(tbody, table){
		$(tbody).on("click", "button.eliminar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var idusuario = $("#frmEliminar #idPregunta").val( data.idpregunta );
		});
	}
	
	
	
	var obtener_data_editar = function(tbody, table){
		$(tbody).on("click", "button.editar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var nameSeccion = $("#name2").val(data.nombrePregunta);
			var puntaje = $("#puntaje2").val(data.puntaje);
			var puntaje = $("#descripcion2").val(data.descripcion);
			var idPregunta = $(" #frmEditar #idPregunta").val( data.idpregunta );
		});
	}
		
	var eliminar = function(){
		$("#eliminar-fondo").on("click", function(){
			var idPregunta = $("#frmEliminar #idPregunta").val()
			$.ajax({
				method:"POST",
				url: "../../sourcePHP/formularios/preguntas/removePregunta.php",
				data: {
						"idPregunta": idPregunta
					  }
			}).done( function( info ){
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				listar();
			});
		});
	}
	
	var guardar = function(){
		$("#agregar-fondo").on("click", function(){			
			var valor_llave= getQuerystring('dataSeccion');
			var nameSeccion = $("#frmAgregar #name").val();
			var puntaje = $("#frmAgregar #puntaje").val();
			var descripcion = $("#frmAgregar #descripcion").val();			
				
			$.ajax({
				method: "POST",
				url: "../../sourcePHP/formularios/preguntas/addPregunta.php",
				data: {
						"name"   : nameSeccion,						
						"puntaje" : puntaje,
						"descripcion" : descripcion,
						"seccion"  : valor_llave
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				limpiar_datos();
				listar();
			});
		});
	}
	
	var editar = function(){
		$("#editar-fondo").on("click", function(){			
			
			var namePregunta = $("#frmEditar #name2").val();
			var puntaje = $("#frmEditar #puntaje2").val();
			var idPregunta = $("#frmEditar #idPregunta").val();
			var descripcion = $("#frmEditar #descripcion2").val();
			
			$.ajax({
				method: "POST",
				url: "../../sourcePHP/formularios/preguntas/editarPregunta.php",
				data: {
						"name2"   : namePregunta,						
						"puntaje2" : puntaje,
						"descripcion2" : descripcion,
						"idPregunta" : idPregunta
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				limpiar_datos2();
				listar();
			});
		});
	}
	var mostrar_mensaje = function( informacion ){
		
		var texto = "", color = "";
		if( informacion.respuesta == "BIEN" ){
			texto = "<strong>Bien!</strong> Se han guardado los cambios correctamente.";
			color = "#379911";
		}else if( informacion.respuesta == "ERROR"){
			texto = "<strong>Error</strong>, no se ejecutó la consulta.";
			color = "#C9302C";
		}else if( informacion.respuesta == "EXISTE" ){
			texto = "<strong>Información!</strong> el usuario ya existe.";
			color = "#5b94c5";
		}else if( informacion.respuesta == "VACIO" ){
			texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
			color = "#ddb11d";
		}else if( informacion.respuesta == "OPCION_VACIA" ){
			texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
			color = "#ddb11d";
		}

		$(".mensaje").html( texto ).css({"color": color });
		$(".mensaje").fadeOut(5000, function(){
			$(this).html("");
			$(this).fadeIn(3000);
		});			
	}
		
	var limpiar_datos = function(){			
			$("#name").val("");
			$("#tipo").val("");
			$("#puntaje").val("");
			$("#descripcion").val("");
			
	}
	
	var limpiar_datos2 = function(){			
			$("#name2").val("");
			$("#puntaje2").val("");			
			
	}
			
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}
