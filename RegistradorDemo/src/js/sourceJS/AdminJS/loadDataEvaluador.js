$(document).on("ready", function(){
	listar();
	eliminar();
	guardar();
	editar();

});

	var listar = function(){
	
		var table = $("#evaluador").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				"url": "../../sourcePHP/evaluador/showEvaluador.php"
			},
			"columns":[
				{"data":"nombreCompleto"},
				{"data":"correo"},
				{"data":"telefono"},
				{"data":"descripcion"},
				{"data":"nombreUsuario"},
				{"data":"password"},			
				
				{"defaultContent": "<button type='button' class='editar btn btn-primary' data-toggle='modal' data-target='#myModalEditar'><i class='fa fa-pencil-square-o'></i></button>	<button type='button' class='eliminar btn btn-danger' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-trash-o'></i></button>"}	
			],
			"language": idioma_espanol
		});
		
		obtener_id_eliminar("#evaluador tbody", table);
		obtener_data_editar("#evaluador tbody", table);
	}
		
	var obtener_id_eliminar = function(tbody, table){
		$(tbody).on("click", "button.eliminar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var idperfil = $("#frmEliminar #idevaluador").val( data.idevaluador );
		});
	}
	
	var obtener_data_editar = function(tbody, table){
		$(tbody).on("click", "button.editar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var nombreCompleto = $("#name2").val(data.nombreCompleto);			
			var email = $("#email22").val(data.correo);	
			var username = $("#username2").val(data.nombreUsuario);		
			var descripcion = $("#textarea2").val(data.descripcion);			
			var telefono = $("#telephone2").val(data.telefono);	
			var idEvaluador = $("#idevaluador").val(data.idevaluador);		
			
		});
	}
		
	var eliminar = function(){
		$("#eliminar-fondo").on("click", function(){
			var idevaluador = $("#frmEliminar #idevaluador").val()
			$.ajax({
				method:"POST",
				url: "../../sourcePHP/evaluador/removeEvaluador.php",
				data: {
						"idevaluador": idevaluador
					  }
			}).done( function( info ){
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				listar();
			});
		});
	}
	
	var guardar = function(){
		$("#agregar-usuario").on("click", function(){			
			
			var name = $("#frmAgregar #name").val();			
			var correo = $("#frmAgregar #email").val();			
			var username = $("#frmAgregar #username").val();			
			var telefono = $("#frmAgregar #telephone").val();			
			var password = $("#frmAgregar #password").val();			
			var descripcion = $("#frmAgregar #textarea").val();			
				
			$.ajax({
				method: "POST",
				url: "../../sourcePHP/evaluador/addEvaluador.php",
				data: {
						"name"   	  : name,
						"correo"   	  : correo,
						"username" 	  : username,
						"telefono"	  : telefono,
						"password"	  : password,
						"descripcion" : descripcion
												
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				limpiar_datos();
				listar();
			});
		});
	}
	
	var editar = function(){
		$("#editar-usuario").on("click", function(){			
			
			var idevaluador = $("#frmEditar #idevaluador").val();		
			var name = $("#frmEditar #name2").val();
			var email = $("#frmEditar #email22").val();
			var usuario = $("#frmEditar #username2").val();
			var telefono = $("#frmEditar #telephone2").val();
			var descripcion = $("#frmEditar #textarea2").val();
					
			$.ajax({
				method: "POST",
				url: "../../sourcePHP/evaluador/editarEvaluador.php",
				data: {
						"idevaluador"   : idevaluador,						
						"name" : name,
						"email" : email,
						"usuario" : usuario,
						"telefono" : telefono,
						"descripcion" : descripcion
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				limpiar_datos2();
				listar();
			});
		});
	}
	var mostrar_mensaje = function( informacion ){
		var texto = "", color = "";
			if( informacion.respuesta == "BIEN" ){
				texto = "<strong>Bien!</strong> Se han guardado los cambios correctamente.";
				color = "#379911";
			}else if( informacion.respuesta == "ERROR"){
				texto = "<strong>Error</strong>, no se ejecutó la consulta.";
				color = "#C9302C";
			}else if( informacion.respuesta == "EXISTE" ){
				texto = "<strong>Información!</strong> el usuario ya existe.";
				color = "#5b94c5";
			}else if( informacion.respuesta == "VACIO" ){
				texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
				color = "#ddb11d";
			}else if( informacion.respuesta == "OPCION_VACIA" ){
				texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
				color = "#ddb11d";
			}

			$(".mensaje").html( texto ).css({"color": color });
			$(".mensaje").fadeOut(5000, function(){
				$(this).html("");
				$(this).fadeIn(3000);
			});			
		}
	
	var limpiar_datos = function(){			
			$("#name").val("");
			$("#email").val("");
			$("#email2").val("");
			$("#username").val("");
			$("#password").val("");
			$("#password2").val("");
			$("#telephone").val("");
			$("#textarea").val("");
			
	}
	
	var limpiar_datos2 = function(){			
			$("#name2").val("");
			$("#fechaI2").val("");
			$("#fechaF2").val("");
			
	}
			
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}
