$(document).on("ready", function(){
	listar();
	eliminar();
	guardar();
	editar();

});

	var listar = function(){
	
		var table = $("#perfiles").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				"url": "../../sourcePHP/perfiles/showPerfil.php"
			},
			"columns":[
				{"data":"nombrePerfil"},				
				{"defaultContent": "<button type='button' class='editar btn btn-primary' data-toggle='modal' data-target='#myModalEditar'><i class='fa fa-pencil-square-o'></i></button>	<button type='button' class='eliminar btn btn-danger' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-trash-o'></i></button>"}	
			],
			"language": idioma_espanol
		});
		
		obtener_id_eliminar("#perfiles tbody", table);
		obtener_data_editar("#perfiles tbody", table);
	}
		
	var obtener_id_eliminar = function(tbody, table){
		$(tbody).on("click", "button.eliminar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var idperfil = $("#frmEliminar #idperfil").val( data.idperfil );
		});
	}
	
	var obtener_data_editar = function(tbody, table){
		$(tbody).on("click", "button.editar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var nombrePerfil = $("#name2").val(data.nombrePerfil);			
			var idperfil = $("#idperfil").val( data.idperfil );			
		});
	}
		
	var eliminar = function(){
		$("#eliminar-fondo").on("click", function(){
			var idperfil = $("#frmEliminar #idperfil").val()
			$.ajax({
				method:"POST",
				url: "../../sourcePHP/perfiles/removePerfil.php",
				data: {
						"idperfil": idperfil
					  }
			}).done( function( info ){
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				listar();
			});
		});
	}
	
	var guardar = function(){
		$("#agregar-fondo").on("click", function(){			
			
			var nameFondo = $("#frmAgregar #name").val();			
				
			$.ajax({
				method: "POST",
				url: "../../sourcePHP/perfiles/addPerfil.php",
				data: {
						"name"   : nameFondo						
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				limpiar_datos();
				listar();
			});
		});
	}
	
	var editar = function(){
		$("#editar-fondo").on("click", function(){			
			
			var nombrePerfil = $("#frmEditar #name2").val();		
			var idperfil = $("#frmEditar #idperfil").val(); 	
			
			$.ajax({
				method: "POST",
				url: "../../sourcePHP/perfiles/editarPerfil.php",
				data: {
						"name2"   : nombrePerfil,						
						"idperfil" : idperfil
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				limpiar_datos2();
				listar();
			});
		});
	}
	var mostrar_mensaje = function( informacion ){
		var texto = "", color = "";
			if( informacion.respuesta == "BIEN" ){
				texto = "<strong>Bien!</strong> Se han guardado los cambios correctamente.";
				color = "#379911";
			}else if( informacion.respuesta == "ERROR"){
				texto = "<strong>Error</strong>, no se ejecutó la consulta.";
				color = "#C9302C";
			}else if( informacion.respuesta == "EXISTE" ){
				texto = "<strong>Información!</strong> el usuario ya existe.";
				color = "#5b94c5";
			}else if( informacion.respuesta == "VACIO" ){
				texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
				color = "#ddb11d";
			}else if( informacion.respuesta == "OPCION_VACIA" ){
				texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
				color = "#ddb11d";
			}

			$(".mensaje").html( texto ).css({"color": color });
			$(".mensaje").fadeOut(5000, function(){
				$(this).html("");
				$(this).fadeIn(3000);
			});			
		}
	
	var limpiar_datos = function(){			
			$("#name").val("");
			$("#fechaI").val("");
			$("#fechaF").val("");
			
	}
	
	var limpiar_datos2 = function(){			
			$("#name2").val("");
			$("#fechaI2").val("");
			$("#fechaF2").val("");
			
	}
			
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}
