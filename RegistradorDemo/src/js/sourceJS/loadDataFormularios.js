$(document).on("ready", function(){
	listar();
	eliminar();
	guardar();
	editar();

});

	var listar = function(){
	
		var table = $("#formulario").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				"url": "../../sourcePHP/formularios/showFormulario.php"
			},
			"columns":[
				{"data":"nombreFormulario"},
				{"data":"fechaInicio"},
				{"data":"fechaTermino"},
				{"data":"estado"},
				{"data":"nombreFondoConcursable"},
				{"defaultContent": "<button type='button' class='editar btn btn-primary' data-toggle='modal' data-target='#myModalEditar'><i class='fa fa-pencil-square-o'></i></button>	<button type='button' class='eliminar btn btn-danger' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-trash-o'></i></button> <button type='button' class='detalles btn btn-success'><i class='fa fa-info-circle'></i></button>"}	
			],
			"language": idioma_espanol
		});
		
		obtener_id_eliminar("#formulario tbody", table);
		obtener_data_editar("#formulario tbody", table);
		obtener_id_redireccionar("#formulario tbody", table);
	}
		
	var obtener_id_eliminar = function(tbody, table){
		$(tbody).on("click", "button.eliminar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var idusuario = $("#frmEliminar #idFormulario").val( data.idformularioPostulacion );
		});
	}
	
	var obtener_id_redireccionar = function(tbody, table){
		$(tbody).on("click", "button.detalles", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="http://192.168.0.58/corfoSystem/production/encargadoFormularios/verSecciones.html?data="+data.idformularioPostulacion;
		});
	}
	
	var obtener_data_editar = function(tbody, table){
		$(tbody).on("click", "button.editar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var nameFondo = $("#name2").val(data.nombreFormulario);
			var fechaInicio = $("#fechaI2").val(data.fechaInicio);
			var fechaTermino = $("#fechaF2").val(data.fechaTermino);			
			var idFormulario = $(" #frmEditar #idFormulario").val( data.idformularioPostulacion );
		});
	}
		
	var eliminar = function(){
		$("#eliminar-fondo").on("click", function(){
			var idFormulario = $("#frmEliminar #idFormulario").val()
			$.ajax({
				method:"POST",
				url: "../../sourcePHP/formularios/removeFormulario.php",
				data: {
						"idFormulario": idFormulario
					  }
			}).done( function( info ){
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				location.reload(true);
			});
		});
	}
	
	var guardar = function(){
		$("#agregar-fondo").on("click", function(){			
			
			var nameFondo = $("#frmAgregar #name").val();
			var fechaInicio = $("#frmAgregar #fechaI").val();
			var fechaTermino = $("#frmAgregar #fechaF").val();
			var estado = $("#frmAgregar #estado").val();
			var fondo = $("#frmAgregar #fondo").val();
				
			$.ajax({
				method: "POST",
				url: "../../sourcePHP/formularios/addFormulario.php",
				data: {
						"name"   : nameFondo,
						"fechaI" : fechaInicio,
						"fechaT" : fechaTermino,
						"estado" : estado,
						"fondo"  : fondo
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				limpiar_datos();
				location.reload(true);
			});
		});
	}
	
	var editar = function(){
		$("#editar-fondo").on("click", function(){			
			
			var nameFondo = $("#frmEditar #name2").val();
			var fechaInicio = $("#frmEditar #fechaI2").val();
			var fechaTermino = $("#frmEditar #fechaF2").val();
			var idFormulario = $("#frmEditar #idFormulario").val() 	
			$.ajax({
				method: "POST",
				url: "../../sourcePHP/formularios/editarFormulario.php",
				data: {
						"name2"   : nameFondo,
						"fechaI2" : fechaInicio,
						"fechaT2" : fechaTermino,
						"idFormulario" : idFormulario
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				limpiar_datos2();
				location.reload(true);
			});
		});
	}
	var mostrar_mensaje = function( informacion ){
		
		var texto = "", color = "";
		if( informacion.respuesta == "BIEN" ){
			texto = "<strong>Bien!</strong> Se han guardado los cambios correctamente.";
			color = "#379911";
		}else if( informacion.respuesta == "ERROR"){
			texto = "<strong>Error</strong>, no se ejecutó la consulta.";
			color = "#C9302C";
		}else if( informacion.respuesta == "EXISTE" ){
			texto = "<strong>Información!</strong> el usuario ya existe.";
			color = "#5b94c5";
		}else if( informacion.respuesta == "VACIO" ){
			texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
			color = "#ddb11d";
		}else if( informacion.respuesta == "OPCION_VACIA" ){
			texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
			color = "#ddb11d";
		}

		$(".mensaje").html( texto ).css({"color": color });
		$(".mensaje").fadeOut(5000, function(){
			$(this).html("");
			$(this).fadeIn(3000);
		});			
	}
		
	var limpiar_datos = function(){			
			$("#name").val("");
			$("#fechaI").val("");
			$("#fechaF").val("");
			
	}
	
	var limpiar_datos2 = function(){			
			$("#name2").val("");
			$("#fechaI2").val("");
			$("#fechaF2").val("");
			
	}
			
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}
