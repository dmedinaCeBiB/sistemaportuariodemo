$(document).on("ready", function(){
	listar();

});

	var listar = function(){
		
		var table = $("#zonas").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				url: "../php/viewsInfor/zona/showData.php"
			},
			"columns":[
				{"data":"nombreZona"},
				{"data":"tipoZona"},
				{"data":"nombreInstalacion"},
				{"data":"nombreBahia"},
				{"defaultContent": "<button type='button' class='caracteristica btn btn-success' ><i class='fa fa-ship'></i></button>"},
				{"defaultContent": "<button type='button' class='viewMap btn btn-success'><i class='fa fa-map-marker'></i></button>"}	
			],
			"language": idioma_espanol
		});
		
		redireccionarCaracteristica("#zonas tbody", table);		
		redireccionamientoMapa("#zonas tbody", table);		
	}
	
	/*redireccionamiento a caracteristicas*/
	var redireccionarCaracteristica = function(tbody, table){
		$(tbody).on("click", "button.caracteristica", function(){	
			var data = table.row( $(this).parents("tr") ).data();			
			location.href="especificaciones.html?zona="+data.idzona;
		});
	}
	
	/*redireccionamiento a mapa*/
	var redireccionamientoMapa = function(tbody, table){
		$(tbody).on("click", "button.viewMap", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="map.html?data="+data.idzona;
		});
	}
				
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}


