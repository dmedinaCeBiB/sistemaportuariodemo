$(document).on("ready", function(){
	listarBahia();
	listarInstalacion();

});

	var listarBahia = function(){
		
		var table = $("#documentsBahia").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				url: "../php/viewsInfor/documentos/showDataBahia.php"
			},
			"columns":[
				{"data":"nombreDocumento"},
				{"data":"descripcion"},						
				{"data":"nombreBahia"},
				{"defaultContent": "<button type='button' class='caracteristica btn btn-success' ><i class='fa fa-file-o'></i></button>"}				
			],
			"language": idioma_espanol
		});
				
		redireccionarCaracteristica("#documentsBahia tbody", table);		
	}
	
	/*redireccionamiento a caracteristicas*/
	var redireccionarCaracteristica = function(tbody, table){
		$(tbody).on("click", "button.caracteristica", function(){	
			var data = table.row( $(this).parents("tr") ).data();			
			location.href="../../../resource/bahia/documents/"+data.idbahia+"/"+data.nombreDocumento;
		});
	}
	
	
	var listarInstalacion = function(){
		
		var table = $("#documentsInstalacion").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				url: "../php/viewsInfor/documentos/showDataInstalacion.php"
			},
			"columns":[
				{"data":"nombreDocumento"},
				{"data":"descripcionDocumento"},						
				{"data":"nombreInstalacion"},
				{"data":"nombreBahia"},
				{"defaultContent": "<button type='button' class='caracteristicaI btn btn-success' ><i class='fa fa-file-o'></i></button>"}				
			],
			"language": idioma_espanol
		});
				
		redireccionarCaracteristicaI("#documentsInstalacion tbody", table);		
	}
	
	/*redireccionamiento a caracteristicas*/
	var redireccionarCaracteristicaI = function(tbody, table){
		$(tbody).on("click", "button.caracteristicaI", function(){	
			var data = table.row( $(this).parents("tr") ).data();			
			location.href="../../../resource/instalacion/documents/"+data.idinstalacionPortuaria+"/"+data.nombreDocumento;
		});
	}
					
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}


