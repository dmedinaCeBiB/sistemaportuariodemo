$(document).on("ready", function(){
	listarBahia();
	listarInstalacion();

});

	var listarBahia = function(){
		
		var table = $("#imagenesBahia").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				url: "../php/viewsInfor/imagenes/showDataBahia.php"
			},
			"columns":[
				{"data":"nombreFoto"},
				{"data":"descripcion"},						
				{"data":"nombreBahia"},
				{"defaultContent": "<button type='button' class='caracteristica btn btn-success' ><i class='fa fa-file-o'></i></button>"}				
			],
			"language": idioma_espanol
		});
				
		redireccionarCaracteristica("#imagenesBahia tbody", table);		
	}
	
	/*redireccionamiento a caracteristicas*/
	var redireccionarCaracteristica = function(tbody, table){
		$(tbody).on("click", "button.caracteristica", function(){	
			var data = table.row( $(this).parents("tr") ).data();			
			location.href="../../../resource/bahia/images/"+data.idbahia+"/"+data.nombreFoto;
		});
	}
	
	
	var listarInstalacion = function(){
		
		var table = $("#imagenesInstalacion").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				url: "../php/viewsInfor/imagenes/showDataInstalacion.php"
			},
			"columns":[
				{"data":"nombreImagen"},
				{"data":"descripcion"},						
				{"data":"nombreInstalacion"},
				{"data":"nombreBahia"},
				{"defaultContent": "<button type='button' class='caracteristicaI btn btn-success' ><i class='fa fa-file-o'></i></button>"}				
			],
			"language": idioma_espanol
		});
				
		redireccionarCaracteristicaI("#imagenesInstalacion tbody", table);		
	}
	
	/*redireccionamiento a caracteristicas*/
	var redireccionarCaracteristicaI = function(tbody, table){
		$(tbody).on("click", "button.caracteristicaI", function(){	
			var data = table.row( $(this).parents("tr") ).data();			
			location.href="../../../resource/instalacion/images/"+data.idinstalacionPortuaria+"/"+data.nombreImagen;
		});
	}
					
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}


