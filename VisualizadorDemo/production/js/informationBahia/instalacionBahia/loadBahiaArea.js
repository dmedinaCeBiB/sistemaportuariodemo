$(document).on("ready", function(){
	listar();

});

	var listar = function(){
		
		var dataID = getQuerystring("data");
		var table = $("#areas").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				url: "../../php/bahiaInstalacion/instalacionArea/showData.php?id="+dataID
			},
			"columns":[
				{"data":"nombreArea"},
				{"data":"nombreInstalacion"},		
				{"data":"nombreBahia"},		
				{"defaultContent": "<button type='button' class='caracteristica btn btn-success' ><i class='fa fa-ship'></i></button> <button type='button' class='georeference btn btn-success' ><i class='fa fa-map-marker'></i></button>"}
			],
			"language": idioma_espanol
		});
		
		
		/*redireccionamiento*/
		redireccionarCaracteristica("#areas tbody", table);		
		redireccionarGeoref("#areas tbody", table);	
	}
	
	//funcion para recuperar la clave del valor obtenido por paso de referencia
	function getQuerystring(key, default_) {
		if (default_ == null)
			default_ = "";
		key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regex = new RegExp("[\\?&amp;]"+key+"=([^&amp;#]*)");
		var qs = regex.exec(window.location.href);
		if(qs == null)
			return default_;
		else
			return qs[1];
	};
	
	/*redireccionamiento a caracteristicas*/
	var redireccionarCaracteristica = function(tbody, table){
		$(tbody).on("click", "button.caracteristica", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="caracteristicas.html?data="+data.idareaManiobra;
		});
	}
	
	/*redireccionamiento a informacion georeferencia*/
	var redireccionarGeoref = function(tbody, table){
		$(tbody).on("click", "button.georeference", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="georef.html?data="+data.idareaManiobra;
		});
	}
			
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}


