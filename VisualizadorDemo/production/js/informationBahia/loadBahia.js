$(document).on("ready", function(){
	listar();

});

	var listar = function(){
	
		var table = $("#bahiaCostera").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				"url": "../php/bahia/showData.php"
			},
			"columns":[
				{"data":"nombreBahia"},
				{"defaultContent": "<button type='button' class='caracteristica btn btn-success' ><i class='fa fa-ship'></i></button>"},				
				{"defaultContent": "<button type='button' class='georeference btn btn-success' ><i class='fa fa-map-marker'></i></button>"},
				{"defaultContent": "<button type='button' class='fichaTecnica btn btn-success' ><i class='fa fa-calendar-check-o'></i></button>"},
				{"defaultContent": "<button type='button' class='documentos btn btn-success' ><i class='fa fa-archive'></i></button>"},
				{"defaultContent": "<button type='button' class='galeria btn btn-success' ><i class='fa fa-camera-retro'></i></button>"},
				{"defaultContent": "<button type='button' class='instalaciones btn btn-success' ><i class='fa fa-anchor'></i></button>"},
				{"defaultContent": "<button type='button' class='viewMap btn btn-success' ><i class='fa fa-globe'></i></button>"}
				
			],
			"language": idioma_espanol
		});
		
		/*redireccionamiento*/
		redireccionarCaracteristica("#bahiaCostera tbody", table);		
		redireccionarInfoGeo("#bahiaCostera tbody", table);		
		redireccionarFichaTec("#bahiaCostera tbody", table);		
		redireccionarDocumentos("#bahiaCostera tbody", table);		
		redireccionarGaleria("#bahiaCostera tbody", table);		
		redireccionarInstalacion("#bahiaCostera tbody", table);		
	}
	
	/*redireccionamiento a caracteristicas*/
	var redireccionarCaracteristica = function(tbody, table){
		$(tbody).on("click", "button.caracteristica", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../bahiaCaracteristica/index.html?data="+data.idbahia;
		});
	}
	
	/*redireccionamiento a informacion geografica*/
	var redireccionarInfoGeo = function(tbody, table){
		$(tbody).on("click", "button.georeference", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../bahiaGeo/index.html?data="+data.idbahia;
		});
	}
	
	/*redireccionamiento a ficha tecnica*/
	var redireccionarFichaTec = function(tbody, table){
		$(tbody).on("click", "button.fichaTecnica", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../bahiaFicha/index.html?data="+data.idbahia;
		});
	}
	
	/*redireccionamiento a documentos*/
	var redireccionarDocumentos = function(tbody, table){
		$(tbody).on("click", "button.documentos", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../bahiaDocumento/index.html?data="+data.idbahia;
		});
	}
	
	/*redireccionamiento a galeria*/
	var redireccionarGaleria = function(tbody, table){
		$(tbody).on("click", "button.galeria", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../bahiaGaleria/index.html?data="+data.idbahia;
		});
	}
	
	/*redireccionamiento a instalaciones*/
	var redireccionarInstalacion = function(tbody, table){
		$(tbody).on("click", "button.instalaciones", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="../bahiaInstalacion/index.html?data="+data.idbahia;
		});
	}
	
	/*redireccionamiento a mapa*/
	var redireccionarInstalacion = function(tbody, table){
		$(tbody).on("click", "button.viewMap", function(){	
			var data = table.row( $(this).parents("tr") ).data();
			location.href="map.html?idData="+data.idbahia;
		});
	}		
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}

