$(document).on("ready", function(){
	listar();
	eliminar();
	guardar();
	editar();

});

	var listar = function(){
	
		var table = $("#fondosTable").DataTable({
			"destroy":true,
			"ajax":{
				"method":"POST",
				"url": "../../sourcePHP/fondosConcursables/showFondos.php"
			},
			"columns":[
				{"data":"nombreFondoConcursable"},
				{"data":"fechaInicio"},
				{"data":"fechaTermino"},
				{"defaultContent": "<button type='button' class='editar btn btn-primary' data-toggle='modal' data-target='#myModalEditar'><i class='fa fa-pencil-square-o'></i></button>	<button type='button' class='eliminar btn btn-danger' data-toggle='modal' data-target='#modalEliminar' ><i class='fa fa-trash-o'></i></button>"}	
			],
			"language": idioma_espanol
		});
		
		obtener_id_eliminar("#fondosTable tbody", table);
		obtener_data_editar("#fondosTable tbody", table);
	}
		
	var obtener_id_eliminar = function(tbody, table){
		$(tbody).on("click", "button.eliminar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var idusuario = $("#frmEliminar #idfondo").val( data.idfondoConcursable );
		});
	}
	
	var obtener_data_editar = function(tbody, table){
		$(tbody).on("click", "button.editar", function(){
			var data = table.row( $(this).parents("tr") ).data();
			var nameFondo = $("#name2").val(data.nombreFondoConcursable);
			var fechaInicio = $("#fechaI2").val(data.fechaInicio);
			var fechaTermino = $("#fechaF2").val(data.fechaTermino);			
			var idfondo = $("#idfondo").val( data.idfondoConcursable );			
		});
	}
		
	var eliminar = function(){
		$("#eliminar-fondo").on("click", function(){
			var idusuario = $("#frmEliminar #idfondo").val()
			$.ajax({
				method:"POST",
				url: "../../sourcePHP/fondosConcursables/removeFondo.php",
				data: {
						"idfondo": idusuario
					  }
			}).done( function( info ){
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				location.reload(true);
			});
		});
	}
	
	var guardar = function(){
		$("#agregar-fondo").on("click", function(){			
			
			var nameFondo = $("#frmAgregar #name").val();
			var fechaInicio = $("#frmAgregar #fechaI").val();
			var fechaTermino = $("#frmAgregar #fechaF").val();
				
			$.ajax({
				method: "POST",
				url: "../../sourcePHP/fondosConcursables/addFondo.php",
				data: {
						"name"   : nameFondo,
						"fechaI" : fechaInicio,
						"fechaT" : fechaTermino,
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				limpiar_datos();
				location.reload(true);			
			});
		});
	}
	
	var editar = function(){
		$("#editar-fondo").on("click", function(){			
			
			var nameFondo = $("#frmEditar #name2").val();
			var fechaInicio = $("#frmEditar #fechaI2").val();
			var fechaTermino = $("#frmEditar #fechaF2").val();
			var idusuario = $("#frmEditar #idfondo").val() 	
			$.ajax({
				method: "POST",
				url: "../../sourcePHP/fondosConcursables/editFondo.php",
				data: {
						"name2"   : nameFondo,
						"fechaI2" : fechaInicio,
						"fechaT2" : fechaTermino,
						"idfondo" : idusuario
					}
					
			}).done( function( info ){
				
				var json_info = JSON.parse( info );
				mostrar_mensaje( json_info );
				limpiar_datos2();
				location.reload(true);				
			});
		});
	}
	var mostrar_mensaje = function( informacion ){
		var texto = "", color = "";
			if( informacion.respuesta == "BIEN" ){
				texto = "<strong>Bien!</strong> Se han guardado los cambios correctamente.";
				color = "#379911";
			}else if( informacion.respuesta == "ERROR"){
				texto = "<strong>Error</strong>, no se ejecutó la consulta.";
				color = "#C9302C";
			}else if( informacion.respuesta == "EXISTE" ){
				texto = "<strong>Información!</strong> el usuario ya existe.";
				color = "#5b94c5";
			}else if( informacion.respuesta == "VACIO" ){
				texto = "<strong>Advertencia!</strong> debe llenar todos los campos solicitados.";
				color = "#ddb11d";
			}else if( informacion.respuesta == "OPCION_VACIA" ){
				texto = "<strong>Advertencia!</strong> la opción no existe o esta vacia, recargar la página.";
				color = "#ddb11d";
			}

			$(".mensaje").html( texto ).css({"color": color });
			$(".mensaje").fadeOut(5000, function(){
				$(this).html("");
				$(this).fadeIn(3000);
			});			
		}
	
	var limpiar_datos = function(){			
			$("#name").val("");
			$("#fechaI").val("");
			$("#fechaF").val("");
			
	}
	
	var limpiar_datos2 = function(){			
			$("#name2").val("");
			$("#fechaI2").val("");
			$("#fechaF2").val("");
			
	}
			
	var idioma_espanol = {
	    "sProcessing":     "Procesando...",
	    "sLengthMenu":     "Mostrar _MENU_ registros",
	    "sZeroRecords":    "No se encontraron resultados",
	    "sEmptyTable":     "Ningún dato disponible en esta tabla",
	    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	    "sInfoPostFix":    "",
	    "sSearch":         "Buscar:",
	    "sUrl":            "",
	    "sInfoThousands":  ",",
	    "sLoadingRecords": "Cargando...",
	    "oPaginate": {
	        "sFirst":    "Primero",
	        "sLast":     "Último",
	        "sNext":     "Siguiente",
	        "sPrevious": "Anterior"
	    },
	    "oAria": {
	        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	    }
	}
