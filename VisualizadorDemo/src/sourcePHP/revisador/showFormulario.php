<?php

	include ("../connectDB.php");

	$query = "select formularioPostulacion.idformularioPostulacion, formularioPostulacion.nombreFormulario, formularioPostulacion.fechaInicio, formularioPostulacion.fechaTermino, formularioPostulacion.puntajeTotal, fondoConcursable.nombreFondoConcursable, formularioPostulacion.estado from formularioPostulacion join fondoConcursable on (formularioPostulacion.fondoConcursable = fondoConcursable.idfondoConcursable) where formularioPostulacion.visible=1";
	$resultado = mysqli_query($conexion, $query);

	if (!$resultado){
		die("Error");
	}else{
		
		while($data = mysqli_fetch_assoc($resultado)){
		
			$arreglo["data"][] = $data;
		}
		
		echo json_encode($arreglo);
		
	}
	
	mysqli_free_result($resultado);
	mysqli_close($conexion);
	
?>
